package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnIntentSender;
    Button btnUserList;
    Button btnAdapters;
    Button btnLifeCycle;
    Button btnCoder;
    Button btnWebService;
    Button btnSQLite;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();
            switch(buttonResourceId){
                case R.id.btnImgSpinner:
                    startActivity(new Intent(TempMainActivity.this, ImageSpinnerActivity.class));
                    break;
                case R.id.btnIntentSender:
                    startActivity(new Intent(TempMainActivity.this, IntentSenderActivity.class));
                    break;
                case R.id.btnUserList:
                    startActivity(new Intent(TempMainActivity.this, UserListActivity.class));
                    break;
                case R.id.btnAdapters:
                    startActivity(new Intent(TempMainActivity.this, Adapters.class));
                    break;
                case R.id.btnLifeCycle:
                    startActivity(new Intent(TempMainActivity.this, LifeCycleActivity.class));
                    break;
                case R.id.btnCoder:
                    startActivity(new Intent(TempMainActivity.this, CoderActivity.class));
                    break;
                case R.id.btnWebServiceRequest:
                    startActivity(new Intent(TempMainActivity.this, WebServiceActivity.class));
                    break;
                case R.id.btnSqlite:
                    startActivity(new Intent(TempMainActivity.this, SQLiteActivity.class));
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button)findViewById(R.id.btnImgSpinner);
        btnImgSpinner.setOnClickListener(listener);

        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnIntentSender.setOnClickListener(listener);

        btnUserList = (Button)findViewById(R.id.btnUserList);
        btnUserList.setOnClickListener(listener);

        btnAdapters = (Button)findViewById(R.id.btnAdapters);
        btnAdapters.setOnClickListener(listener);

        btnLifeCycle = (Button)findViewById(R.id.btnLifeCycle);
        btnLifeCycle.setOnClickListener(listener);

        btnCoder = (Button)findViewById(R.id.btnCoder);
        btnCoder.setOnClickListener(listener);

        btnWebService = (Button)findViewById(R.id.btnWebServiceRequest);
        btnWebService.setOnClickListener(listener);

        btnSQLite = (Button)findViewById(R.id.btnSqlite);
        btnSQLite.setOnClickListener(listener);



    }


}
