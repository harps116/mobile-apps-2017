package com.example.kadern.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.kadern.samples2017.MySQLiteOpenHelper;
import com.example.kadern.samples2017.SQLiteUserHelper;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;


/**
 * Created by adam on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    SQLiteUserHelper dbHelper;
    SQLiteDatabase db;

    public static final String  TABLE_NAME = "users";
    public static final String  COLUMN_USER_ID = "_id";
    public static final String  COLUMN_USER_FIRST_NAME = "first_name";
    public static final String  COLUMN_USER_EMAIL = "email";
    public static final String  COLUMN_ACTIVE = "active";

    public static final String TABLE_CREATE = String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT)",
            TABLE_NAME,
            COLUMN_USER_ID,
            COLUMN_USER_FIRST_NAME,
            COLUMN_USER_EMAIL,
            COLUMN_ACTIVE);

    public UserDataAccess(SQLiteUserHelper dbHelper){
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_FIRST_NAME, i.getFirstName());
        values.put(COLUMN_USER_EMAIL, i.getEmail());
        values.put(COLUMN_ACTIVE, i.isActive());
        long insertId = db.insert(TABLE_NAME, null, values);
        return i;
    }

    public ArrayList<User> getAllUsers(){
        ArrayList<User> items = new ArrayList<User>();
        String query = String.format("SELECT %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_FIRST_NAME, TABLE_NAME);
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){

            User i = new User(c.getLong(0), c.getString(1), c.getString(2), c.getInt(3), c.getInt(4) != 0);
            items.add(i);
            c.moveToNext();
        }
        c.close();
        return items;
    }

}
